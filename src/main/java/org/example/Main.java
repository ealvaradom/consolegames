package org.example;
import java.util.Scanner;


import org.example.games.TicTacToe;
import org.example.games.hangman.Hangman;

public class Main {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose a game to play:");
        System.out.println("1. Tic-Tac-Toe.");
        System.out.println("2. Hangman.");
        System.out.println("Input your game number:");
        int gameInput = Integer.parseInt(scanner.nextLine());

        switch (gameInput){
            case 1: startTicTacToe(scanner);
                    break;
            case 2: startHangman(scanner);
                    break;
            default : break;
        }

        scanner.close();

    }

    private static void startTicTacToe(Scanner scanner){
        TicTacToe ticTacToe =  new TicTacToe();
        ticTacToe.startGame(scanner);
    }

    private static void startHangman(Scanner scanner){
        Hangman hangman =  new Hangman();
        hangman.startGame(scanner);
    }

}