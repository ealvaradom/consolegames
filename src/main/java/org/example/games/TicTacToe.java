package org.example.games;
import java.util.Scanner;
public class TicTacToe {

    private final String PLAYER_X = "X";
    private final String PLAYER_O = "O";

    public void startGame (Scanner scanner){

        String[][] board = new String[3][3];
        initializeBoard(board);

        String currentPlayer = setupPlayerToken(scanner);
        System.out.println(printBoard(board));

        do {
            System.out.println("PLAYER :" + currentPlayer);

            int xInput = getInput(scanner,"X");
            int yInput = getInput(scanner,"Y");

            if (isValidMove(board, xInput, yInput)) {
                board[xInput][yInput] = currentPlayer;
                System.out.println(printBoard(board));
                currentPlayer = switchPlayer(currentPlayer);
            } else {
                System.out.println("Invalid move. Try again.");
            }
        } while (!isGameOver(board));

        System.out.println("GAME WINNER: PLAYER WITH TOKEN " + changeToken(currentPlayer));

    }

    private boolean isValidToken(String input){
        return input.equals(PLAYER_X) || input.equals(PLAYER_O);
    }
    private String changeToken(String currentPlayer){
        return currentPlayer.equals(PLAYER_X) ? PLAYER_O : PLAYER_X;
    }

    private String setBoardToEvaluate(String[][] board){

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j].equals(" ")) {
                    result.append("N");
                } else {
                    result.append(board[i][j]);
                }            }
        }

        return result.toString();
    }

    private boolean isGameOver(String[][] board) {
        String finalBoard = setBoardToEvaluate(board);
        return isXTheWinner(finalBoard) || isOTheWinner(finalBoard);
    }

    private boolean isXTheWinner(String board){
        String finalBoard = board.replace(PLAYER_O, "N");

        // Define winning combinations
        String[] winningCombinations = {
                "XXXNNNNNN", "NNNXXXNNN", "NNNNNNXXX",
                "XNNXNNXNN", "NXNNXNNXN", "NNXNNXNNX",
                "XNNNXNNNX", "NNXNXNXNN"
        };

        // Check if any winning combination is present
        for (String combination : winningCombinations) {
            if (finalBoard.equalsIgnoreCase(combination)) {
                return true;
            }
        }

        return false;
    }

    private boolean isOTheWinner(String board){
        String finalBoard = board.replace(PLAYER_X, "N");

        // Define winning combinations
        String[] winningCombinations = {
                "OOONNNNNN", "NNNOOONNN", "NNNNNNOOO",
                "ONNONNONN", "NONNONNON", "NNONNONNO",
                "ONNNONNNO", "NNONONONN"
        };

        // Check if any winning combination is present
        for (String combination : winningCombinations) {
            if (finalBoard.equalsIgnoreCase(combination)) {
                return true;
            }
        }

        return false;
    }

    private void initializeBoard(String[][] board){
        for(int i = 0; i < board.length; i++){
            for (int j = 0; j < board[i].length; j++){
                board[i][j] = " ";
            }
        }
    }

    private String printBoard(String[][] board){
        StringBuilder result = new StringBuilder("  Y| 0 | 1 | 2 \n");
        result.append("_______________\n");
        for (int i = 0; i < board.length; i++) {
            result.append("X").append(i).append(" | ");
            for (int j = 0; j < board[i].length; j++) {
                result.append(board[i][j]).append(" | ");
            }
            result.append("\n_______________\n");
        }
        return result.toString();

    }

    private String setupPlayerToken(Scanner scanner){
        System.out.println("===============TIC-TAC-TOE GAME.==================");
        System.out.println("First player choose a token for playing(X or O).");
        String currentPlayer;
        do {
            currentPlayer = scanner.nextLine().toUpperCase();
        } while (!isValidToken(currentPlayer));

        System.out.println("Your token: " + currentPlayer);
        return currentPlayer;
    }
    private int getInput(Scanner scanner, String axis) {
        int input;
        do {
            System.out.println("Enter a value for " + axis + ": ");
            try {
                input = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number.");
                input = -1;  // Set to an invalid value to repeat the loop
            }
        } while (input < 0 || input > 2);

        return input;
    }

    private String switchPlayer(String currentPlayer) {
        return currentPlayer.equals(PLAYER_X) ? PLAYER_O : PLAYER_X;
    }

    private boolean isValidMove(String[][] board, int x, int y) {
        return board[x][y].equals(" ");

    }

}
