package org.example.games.hangman;

import java.util.Scanner;

public class Hangman {

    private String WORD;
    private String USED_LETTERS = "";
    private int GAME_STAGE = 0;
    public void startGame(Scanner scanner){

        RandomWordPicker wordPicker = new RandomWordPicker();
        WORD = wordPicker.getRandomWord().toUpperCase();

        String currentLetter;

        String playerWord = " ".repeat(WORD.length());
        playerWord = playerWord.replace(" ", "_");
        do {
            System.out.println (boardSetup() + wordSetup(playerWord));
            System.out.println("Enter a letter: ");
            currentLetter = scanner.nextLine().toUpperCase();
            if (isValidLetter(currentLetter)) {
                playerWord = assignLetter(currentLetter, playerWord);
            } else {
                System.out.println("Invalid value. Try again.");
            }

        } while (!isGameOver(playerWord));
        if( GAME_STAGE!=9 ){
            System.out.println("YOU WIN!!!");
        }else{
            System.out.println(stage9());
        }
        System.out.println("The word was: "+ WORD);

    }
    private String assignLetter(String currentLetter, String playerWord){

        USED_LETTERS = USED_LETTERS + " | " + currentLetter;
        int length = WORD.length();

        StringBuilder stringBuilder = new StringBuilder(playerWord);
        if (!WORD.contains(currentLetter)){
            GAME_STAGE++;
        }
        else{
        for (int i = 0; i < length; i++) {
            char currentChar = WORD.charAt(i);

            if (currentChar == currentLetter.charAt(0)) {
                stringBuilder.setCharAt(i, currentChar);
            }
        }
        }
    return stringBuilder.toString();

    }

    private boolean isValidLetter(String letter){
        return letter.length() == 1 && !letter.isBlank() && !USED_LETTERS.contains(letter) ;
    }
    private String boardSetup(){

        return switch (GAME_STAGE) {
            case 0 -> stage0();
            case 1 -> stage1();
            case 2 -> stage2();
            case 3 -> stage3();
            case 4 -> stage4();
            case 5 -> stage5();
            case 6 -> stage6();
            case 7 -> stage7();
            case 8 -> stage8();
            default -> "";
        };
    }
    private boolean isGameOver(String playerWord){
        return GAME_STAGE == 9 || WORD.equals(playerWord);
    }

    private String wordSetup(String playerWord){
        StringBuilder stringBuilder = new StringBuilder();

        int wordSize = playerWord.length();
        int marginSize = (20 - wordSize)/2;
        if (wordSize % 2 == 0){
            stringBuilder.append(" ".repeat(marginSize)).append(playerWord).append(" ".repeat(marginSize)).append("\n");
        } else {
            stringBuilder.append(" ".repeat(marginSize + 1)).append(playerWord).append(" ".repeat(marginSize)).append("\n");
        }

        stringBuilder.append("++++++++++++++++++++\n");
        stringBuilder.append("Used Letter:\n");
        stringBuilder.append(USED_LETTERS);
        return stringBuilder.toString();

    }
    private String stage0(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("                    \n");
        stringBuilder.append("                    \n");
        stringBuilder.append("                    \n");
        stringBuilder.append("                    \n");
        stringBuilder.append("                    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage1(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("                    \n");
        stringBuilder.append("               T    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }


    private String stage2(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("               T    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage3(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     |         |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage4(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     O         |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage5(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     O         |    \n");
        stringBuilder.append("     |         |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage6(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     O         |    \n");
        stringBuilder.append("     |-        |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage7(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     O         |    \n");
        stringBuilder.append("    -|-        |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage8(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |         T    \n");
        stringBuilder.append("     O         |    \n");
        stringBuilder.append("    -|-        |    \n");
        stringBuilder.append("      \\        |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }

    private String stage9(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("     ___________    \n");
        stringBuilder.append("     |   GAME  T    \n");
        stringBuilder.append("     @   OVER  |    \n");
        stringBuilder.append("    -|-        |    \n");
        stringBuilder.append("    / \\        |    \n");
        stringBuilder.append("               |    \n");
        stringBuilder.append("====================\n");

        return stringBuilder.toString();
    }
}
